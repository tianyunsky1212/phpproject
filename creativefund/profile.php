<?php

if (false) {
    $app = new \Slim\Slim();
    $log = new \Monolog\Logger('main');
}


// EDIT PROFILE
$app->get('/edit_profile/:email', function($email = "") use ($app) {
    $user = DB::queryFirstRow("SELECT firstName, lastName, email, username FROM users WHERE email=%s", $email);
    if ($user) {
        $app->render('register.html.twig', array(
            'u' => $user,
            'sessionUser' => $_SESSION['user']));
    } else {
        $app->render('login.html.twig');
    }
});
