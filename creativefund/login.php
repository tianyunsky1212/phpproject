<?php

if (false) {
    $app = new \Slim\Slim();
    $log = new \Monolog\Logger('main');
}

// LOGIN
$app->get('/login', function() use ($app) {
    $app->render('login.html.twig');
});


$app->post('/login', function() use ($app) {
    $username = $app->request()->post('username');
    $password = $app->request()->post('password');
    
    $valueList = array('username' =>$username);
    
    $isLoginSuccessful = false;
    
    $user = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);
    if ($user && ($user['password'] == $password)) {
        $isLoginSuccessful = true;
    }
    
    if ($isLoginSuccessful) {
        unset($user['password']);
        $_SESSION['user'] = $user;
        $app->render('index.html.twig', array(
            'sessionUser' => $_SESSION['user']));
    } else {
        $app->render('login.html.twig', array('error' => true, 'u' => $valueList));
    }
});


// LOGOUT
$app->get('/logout', function() use ($app) {
    $_SESSION['user'] = array();
    $app->render('index.html.twig', array('sessionUser' => $_SESSION['user']));
});