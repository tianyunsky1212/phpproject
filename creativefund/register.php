<?php

// REGISTER
$app->get('/register', function() use ($app) {
    $app->render('register.html.twig');
});


$app->post('/register', function() use ($app) {
    $firstName = $app->request()->post('firstName');
    $lastName = $app->request()->post('lastName');
    $username = $app->request()->post('username');
    $email = $app->request()->post('email');
    $password1 = $app->request()->post('createPassword');
    $password2 = $app->request()->post('confirmPassword');
    
    $valueList = array(
        'username' => $username,
        'firstName' => $firstName,
        'lastName' => $lastName,
        'email' => $email);
    
    $errorList = array();
    
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
        array_push($errorList, "Email is invalid");
        unset($valueList['email']);
    }
    if ($password1 != $password2) {
        array_push($errorList, "Passwords do not match");
    }
    if (strlen($password1) < 6) {
        array_push($errorList, "Password must be at least 6 characters long");
    }
    
    if (!$errorList) {
        DB::insert('users', array(
            'username' => $username,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'email' => $email,
            'password' => $password1
        ));
        
        $_SESSION['user'] = $valueList;
        $app->render('index.html.twig', array(
            'sessionUser' => $_SESSION['user']
        ));
    } else {
        $app->render('register.html.twig', array(
            'u' => $valueList,
            'errorList' => $errorList
        ));
    }
});