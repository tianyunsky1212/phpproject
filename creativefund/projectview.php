<?php

if (false) {
    $app = new \Slim\Slim();
    $log = new \Monolog\Logger('main');
}


// PROJECT VIEW
$app->get('/project_view/(:id)', function($id = -1) use ($app) {
    
    if ($id != -1) {
        $project = DB::queryFirstRow("SELECT * FROM projects WHERE id=%i", $id);
        if (!$project) {
            $app->notFound();
            return;
        } else {
            if (!$_SESSION['user']) {
                $app->render('project_view.html.twig', array(
                    'p' => $project));
                return;
            } else {
                $app->render('project_view.html.twig', array(
                    'p' => $project,
                    'sessionUser' => $_SESSION['user']));
            }
        }
    }
});


// BACK
$app->get('/back/(:id)', function($id = -1) use ($app) {    
    if ($id != -1) {
        $project = DB::queryFirstRow("SELECT * FROM projects WHERE id=%i", $id);
        if (!$project) {
            $app->notFound();
            return;
        } else {
            if (!$_SESSION['user']) {
                $msg = 'you need to be logged-in to back a project';
                $app->render('login.html.twig', array(
                    'm' => $msg));
                return;
            } else {
                $app->render('back.html.twig', array(
                    'p' => $project,
                    'sessionUser' => $_SESSION['user']));
            }
        }
    }
});

$app->get('/back/:id', function($id) use ($app) {
    $project = DB::queryFirstRow("SELECT p.description, p.goal, p.duration, p.image, p.status, "
            . "p.title, p.video, p.idea, c.categoryName FROM category as c, projects as p WHERE "
            . "p.categoryId = c.id AND p.id=%s", $id);
    $app->render("back.html.twig", array('p' => $project));
});