<?php

session_start();
require_once 'vendor/autoload.php';

if (true) {
    DB::$user = 'creativefund';
    DB::$dbName = 'creativefund';
    DB::$password = 'creativefund';
    DB::$host = 'localhost';
    DB::$port = 3333;
    DB::$encoding = 'utf8';
    DB::$error_handler = 'db_error_handler';
}
//8tg4Lx2Sa8uxkAAH
else{
     DB::$user = 'cp4907_tianyun';
     DB::$dbName = 'cp4907_tianyun';
     DB::$password = 'AsshjPLMXf7sYqdt';
     DB::$encoding = 'utf8';
}

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

if (!isset($_SESSION['user'])) {
  $_SESSION['user'] = array();
}

$twig = $app->view()->getEnvironment();
$twig->addGlobal('sessionUser', $_SESSION['user']);

//define("PRODUCTSPERPAGE", 3);
//define("ROWSPERPAGE", 2);
//define("MAXPAGES", 4);

if (isset($_SESSION['user'])) {
$app->get('/', function() use ($app) {
    
    $categoryList = DB::queryFirstRow('SELECT c.categoryName FROM category as c ' );
    $app->render('index.html.twig', array(
            'categoryList' => $categoryList,
            'sessionUser' => $_SESSION['user'],
            'u' => $_SESSION['user']));
    });
} else {
    $app->get('/', function() use ($app) {
        $categoryList = DB::queryFirstRow('SELECT c.categoryName FROM category as c ' );

        $app->render('index.html.twig', array(
            'categoryList' => $categoryList));
    }); 
}
/*
$app->get('/project/:id', function($id) use ($app) {
    $project = DB::queryFirstRow("SELECT p.title, p.idea, p.description, a.body, u.email authorName " .
                    " FROM articles as a, users as u WHERE a.authorId = u.id AND a.id=%s", $id);
    $app->render("project_view.html.twig", array('a' => $project));
});
 * */
 
if (isset($_SESSION['user'])) {
$app->get('/project/:category', function($category) use ($app) {
   // $start = ((int) $page - 1) * PRODUCTSPERPAGE;
    $project = DB::query("SELECT  p.title,p.idea,p.description,p.status,p.duration,p.imagePath, c.categoryName, u.firstName, u.lastName" .
                    " FROM projects as p, category as c ,users as u WHERE p.categoryId = c.id AND u.id = p.creatorId AND c.categoryName=%s "
                   ,$category);
   // $count = DB::count();
    $categoryName= DB::queryFirstRow("SELECT categoryName FROM category  WHERE categoryName=%s",$category);
    
    
    //Calculating no of pages

   // $maxPages = round($count / PRODUCTSPERPAGE);
     //$pagePrjectList = array();
    /*if (count($project) < PRODUCTSPERPAGE) {
        $pageProjectList = $project;
        $maxPages = 0;
    } else {
        for ($x = 0; $x < PRODUCTSPERPAGE; $x++) {
            $pageProjectList[$x] = $project[$x];
        }
    }
    foreach ($pageProjectList as &$product) {
        $ID = $pro['id'];
        
       $title = $pro['title'];
         $idea = $pro['idea'];
           $description = $pro['description'];
           
        $product['totalReviews'] = $totalReviews;
        $product['stars'] = round($average);
    }
  
    $pagination = array('min' => max(($page - MAXPAGES - 1), 1), 'max' => $maxPages, 'current' => $page);*/
    $app->render('category_view.html.twig', array('p' => $project,'c' => $categoryName,'sessionUser' => $_SESSION['user']));
});}

else {
    $app->get('/project/:category', function($category) use ($app) {
        $project = DB::query("SELECT  p.title,p.idea,p.description,p.status,p.duration,p.imagePath, c.categoryName, u.firstName, u.lastName" .
                    " FROM projects as p, category as c ,users as u WHERE p.categoryId = c.id AND u.id = p.creatorId AND c.categoryName=%s "
                   ,$category);
        // $count = DB::count();
         $categoryName= DB::queryFirstRow("SELECT categoryName FROM category  WHERE categoryName=%s",$category);
        $app->render("category_view.html.twig", array('p' => $project,'c' => $categoryName));
    });
}
    
    
    
    
    
    
    

$app->get('/project/:userId', function($userId) use ($app) {
    
    $project = DB::query("SELECT  p.title,p.idea,p.description,p.status,p.duration,p.imagePath, c.categoryName, u.id" .
                    " FROM projects as p, category as c ,users as u  WHERE p.creatorId = u.id "); 
    
    $app->render('project_user_list.html.twig', array('p' => $project));// 'pages' => $pagination));
});


$app->get('/session', function() {
    echo "<pre>SESSION:\n\n";
    // var_dump($_SESSION);
    print_r($_SESSION);
});


require_once 'projectadd.php';
require_once 'rewardsadd.php';
require_once 'login.php';
require_once 'register.php';
require_once 'projectview.php';
require_once 'profile.php';

$app->run();



