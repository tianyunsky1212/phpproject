<?php

if (false) {
    $app = new \Slim\Slim();
    $log = new \Monolog\Logger('main');
}

$app->get('/rewards/:id', function($id) use ($app, $log) {
    // state 1: first show
   /* if (!$_SESSION['user']) {
        $app->render('access_denied.html.twig');
        return;
    }*/
     $reward = DB::queryFirstRow("SELECT a.rewardTitle, a .rewardDescription, a.rewardAmount "
             . "FROM rewards as a WHERE id=%i", $id);
    if (!$reward) {
        $app->notFound();
        return;
    }
    $app->render('rewards_add_view.html.twig', array('r' => $reward));
});

$app->get('/rewardsadd', function() use ($app,$log) {
   $app->render('rewards_add.html.twig');
});
$app->post('/rewardsadd', function() use ($app,$log) {
  /*  if (!$_SESSION['user']) {
        $app->render('access_denied.html.twig');
        return;
   * */
    $rewardTitle = $app->request()->post('rewardTitle');
    $rewardAmount = $app->request()->post('rewardAmount');
    $rewardDescription =  $app->request()->post('rewardDescription');
   
    if(isset( $_SESSION['projectId'])){
        
    $projectId = $_SESSION['projectId'];
    
    
    
    $valueListReward = array('rewardTitle' => $rewardTitle, 'rewardAmount' => $rewardAmount, 'rewardDescription' =>$rewardDescription);
    // verify submission
    $errorList = array();
    
    if (strlen($rewardTitle) < 2 || strlen($rewardTitle) > 20) {
        array_push($errorList, "Title must be between 2-20 characters long");
    }
   
    if (strlen($rewardDescription) < 2 || strlen($rewardDescription) > 500) {
        array_push($errorList, "Description must be between 2-500 characters long");
    }
    
    if(!(is_numeric($rewardAmount))||($rewardAmount<0)){
         array_push($errorList, "Reward amount  must be  number and more than zero.");
        
    }
   
    
    if (!$errorList) {
        // state 2: successful submission
        DB::insert('rewards', array(
          
            'rewardTitle' => $rewardTitle,
            'rewardAmount' => $rewardAmount,
            'rewardDescription' => $rewardDescription,
            'projectId'=> $projectId
          
        ));
        $rewardId = DB::insertId();
        $app->render('rewards_add_view.html.twig', array('rewardId' => $rewardId,'r'=>$valueListReward));
    } else {
        // state 3: failed submission
        $app->render('rewards_add.html.twig', array(
            'r' => $valueListReward,
            'errorList' => $errorList
                ));
    }
    }
});


