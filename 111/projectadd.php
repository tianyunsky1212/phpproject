<?php
// NOTE: This code must never execute but it helps Netbeans provide auto-completion

if (false) {
    $app = new \Slim\Slim();
    $log = new \Monolog\Logger('main');
}

$app->get('/projectadd', function() use ($app) {
    // state 1: first show
   /* if (!$_SESSION['user']) {
        $app->render('access_denied.html.twig');
        return;
    }*/
    $app->render('project_add.html.twig');
});


$app->post('/projectadd', function() use ($app) {
  /*  if (!$_SESSION['user']) {
        $app->render('access_denied.html.twig');
        return;
    }
   * */
    // receiving submission
    $title = $app->request()->post('title');
    $idea = $app->request()->post('idea');
    $category = isset($_POST['category']) ? $_POST['category'] : false;
    $goal = $app->request()->post('goal');
    $duration = $app->request()->post('duration');
    $description = $app->request()->post('description');
    $status = $app->request()->post('status');
    
    $categoryId = DB::queryFirstField("SELECT c.id FROM category as c WHERE categoryName=%s", $category);
    //$rewardsId = DB::queryOneField("SELECT r.id FROM rewards as r WHERE categoryName=%s", $category);
    
    $valueList = array('title' => $title, 'idea' => $idea, 'category' =>$category,'goal' =>$goal,
       'duration' => $duration,'description' => $description,'status' => $status );
    // verify submission
    $errorList = array();
    if (strlen($title) < 2 || strlen($title) > 30) {
        array_push($errorList, "Title must be between 2-30 characters long");
    }
    if (strlen($idea) < 2 || strlen($idea) > 200) {
        array_push($errorList, "Idea must be between 2-200 characters long");
    }
    if (strlen($description) < 2 || strlen($description) > 5000) {
        array_push($errorList, "Idea must be between 2-5000 characters long");
    }
    
    if(!(is_numeric($goal))||($goal<0)){
         array_push($errorList, "Goal  must be  number and more than zero.");
        
    }
     if(!(is_numeric($duration))||($duration<0)||( $duration>60)){
         array_push($errorList, "Duration must be number and between 0 and 60.");
        
    } 
    $image=$_FILES['image'];
    $imageInfo = getimagesize($image['tmp_name']);
    if(!$imageInfo){
         array_push($errorList, "File does not look like a valid image");
        
    }
    else{
        //never allow'..'(go to last level document)in the file name
    if(strstr($image['name'], '..')){
         array_push($errorList, "File name invalid");
    }    
    
    //only allow select extension
    $ext = strtolower(pathinfo($image['name'],PATHINFO_EXTENSION));
    if(!in_array($ext, array('jpg','jpeg','gif','png'))){
         array_push($errorList, "File extention invalid.");

    }
    
    //do not allow to override existing file
    if (file_exists('uploads/' . $image['name'])){
        array_push($errorList, "File name already exists, refusing to override.");
    }
    }

    if (!$errorList) {
        // state 2: successful submission
        DB::insert('projects', array(
            'creatorId' => "1",// $_SESSION['user']['id'],
            'title' => $title,
            'idea' => $idea,
            'description' => $description,
             'status' => $status,
              'imagePath' => $image['name'],
             'duration' => $duration,
             'categoryId' => $categoryId,
            'goal' => $goal
        ));
        $projectId = DB::insertId();
        
        $_SESSION['projectId'] = DB::insertId();
        
        $app->render('projectadd_success.html.twig',array('projectId' => $projectId));
    } 
    
    else {
        // state 3: failed submission
        $app->render('project_add.html.twig', array(
            'v' => $valueList,
            'errorList' => $errorList
                ));
    }
    
});

