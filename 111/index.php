<?php

session_start();
require_once 'vendor/autoload.php';

if (true) {
    DB::$user = 'creativefund';
    DB::$dbName = 'creativefund';
    DB::$password = '8tg4Lx2Sa8uxkAAH';
    DB::$host = 'localhost';
    DB::$port = 3333;
    DB::$encoding = 'utf8';
    DB::$error_handler = 'db_error_handler';
}
//8tg4Lx2Sa8uxkAAH
else{
     DB::$user = 'cp4907_tianyun';
     DB::$dbName = 'cp4907_tianyun';
     DB::$password = 'AsshjPLMXf7sYqdt';
     DB::$encoding = 'utf8';
}

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

if (!isset($_SESSION['user'])) {
  $_SESSION['user'] = array();
}

$twig = $app->view()->getEnvironment();
$twig->addGlobal('sessionUser', $_SESSION['user']);

$app->get('/', function() use ($app) {
    
    $categoryList = DB::queryFirstRow('SELECT c.categoryName FROM category as c ' );

    $app->render('index.html.twig', array(
        'categoryList' => $categoryList));
   
});

$app->get('/project/:id', function($id) use ($app) {
    $article = DB::queryFirstRow("SELECT a.id, a.creationTime, a.title, a.body, u.email authorName " .
                    " FROM articles as a, users as u WHERE a.authorId = u.id AND a.id=%s", $id);
    $app->render("article_view.html.twig", array('a' => $article));
});



$app->get('/session', function() {
    echo "<pre>SESSION:\n\n";
    // var_dump($_SESSION);
    print_r($_SESSION);
});


require_once 'projectadd.php';
require_once 'rewardsadd.php';


$app->run();



